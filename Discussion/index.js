// [SECTION] While Loop

/*
	Syntax
	while (expression/condition){
		statement

	}
// While the value of count is not equal to 0

*/

let count = 0;

while(count !=5){
	console.log("While: " + count);


	// iteration - increases the value of count after every iteration to stop the loop when it reacher 5	

	//  ++ -> increment, -- -> decrement

	count++;
}


// [SECTION] Do While Loop

// A do-while loop works a  lot like the while loop. But unlike while loops, do-while loops guarantee that the code will be executed at least once.

/*
	Syntax:

	do {
		statement
	} while (expression/condition)
*/

let number = Number(prompt("Give me a number"));

do {

	console.log("Do While: " + number)

	number += 2
} while (number <10)

// [SECTION] For Loops


/*
	Syntax:
	for(initialization; expression/condition; finalExpression) {
			
		statement


	}

*/

for (let count=0; count<=20; count++){
	console.log(count)

}

let myString = "Alex";
// Characters in strings may be counted using the .length property.

console.log(myString.length)

console.log(myString[0])
console.log(myString[1])
console.log(myString[2])
console.log(myString[3])